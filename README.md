# YAGPDB-image-only-channel


## What is this?

This is a script for Yet Another General Purpose Discord Bot (YAGPDB or YAG) that 
deletes messages that aren't images or videos and sends a DM to the user containing the text of their original message.

## How do I use it?

1. In your server's control panel on YAG's website go to: Core -> Custom Commands
2. Create a new custom command
3. Change the trigger type to regex and make the trigger ".\*" (so it will trigger every message)
4. Paste the contents of image_only_channel_script.cs into the response
5. Set the roles/channels
