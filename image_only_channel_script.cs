{{/* 
This script will delete any messages that aren't images / videos and direct message
the user why their message was deleted along with the text content.
*/}}


{{ $postIsValid := false }}

{{ if .Channel.IsThread }}
	{{/* This channel is a thread, don't run any validation */}}
	{{ $postIsValid = true }}

{{ else }}
	{{/* This channel isn't a thread, make sure the content is valid */}}

	{{ if eq (len .Message.Attachments) 0 }}
		{{/* no attachments detected. delete the message */}}
		{{ $postIsValid = false }}
	{{ else }}
		{{/* make sure every attachment is an image or video */}}
		{{ $allAttachmentsAreMedia := true }}
		
		{{/* Loop through every attachment */}}
		{{- range $attatch := .Message.Attachments}}
			{{if ne $attatch.Width 0 }}
				{{/* This is an image */}}
			{{ else }}
				{{/* This attachment is NOT an image */}}
				{{ $allAttachmentsAreMedia = false }}
			{{ end }}
		{{- end}}
		
		{{ $postIsValid = $allAttachmentsAreMedia }}
	{{ end }}
{{ end }}



{{if not $postIsValid }}
	{{/* delete the post */}}
	{{ deleteMessage nil .Message.ID 0 }}
	
	{{/* 
	Determine the text to send back to the user.
	If "" is sent the DM won't send.
	*/}}
	{{ $messageString := .Message.Content }}
	{{if eq $messageString "" }}
		{{ $messageString = "[No Text Content]" }}
	{{end}}
	
	{{/* send a direct message to the offender */}}
	{{$embed := cembed 	
		"title" "Message Deleted" 
		"description" (joinStr "" "<#" .Channel.ID "> is only for pictures and videos. \nTo comment on a picture please create or use a thread on the original post.")
		"color" 4645612 
		"fields" (cslice 
			(sdict "name" "Your Message" "value" $messageString "inline" false)
		)
	}}
	{{ sendDM $embed }}
{{ end }}
